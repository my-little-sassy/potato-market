FROM openjdk:11

WORKDIR /opt

RUN mkdir /opt/log

COPY target/scala-2.13/potato-market.jar /opt/potato-market.jar
COPY client /opt/client

ENV HOST=0.0.0.0 \
	RABBITMQ_URI=amqp://potato:potato@potato-rabbit:5672

CMD java -jar potato-market.jar
