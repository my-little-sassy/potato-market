build:
	sbt assembly
	docker build -t potato-market:latest .

start:
	docker run --rm --name potato-rabbit \
		--env RABBITMQ_DEFAULT_USER="potato" \
		--env RABBITMQ_DEFAULT_PASS="potato" \
		-it -d --hostname potato -p 15672:15672 -p 5672:5672 \
		--network potato-market \
		rabbitmq:3-management
	sleep 5

	docker run --rm -it -p 8080:8080 -d --name potato-market-1 --network potato-market potato-market:latest
	docker run --rm -it -p 8081:8080 -d --name potato-market-2 --network potato-market potato-market:latest
	docker run --rm -it -p 8082:8080 -d --name potato-market-3 --network potato-market potato-market:latest

stop:
	docker stop potato-market-1 potato-market-2 potato-market-3 potato-rabbit

restart: stop start
