import sbtassembly.AssemblyKeys.assemblyJarName
lazy val root = project
  .in(file("."))
  .settings(
    name := "Potato Market",
    version := "0.1.0-SNAPSHOT",

    scalaVersion := "2.13.8",

    assembly / assemblyJarName := "potato-market.jar",

    scalacOptions ++= Seq(
      "-unchecked",
      "-deprecation",
      "-Xfatal-warnings",
    ),

    libraryDependencies ++= Dependencies.cats
      ++ Dependencies.httpServer
      ++ Dependencies.json
      ++ Dependencies.rabbitMQ
      ++ Dependencies.utils
  )
