import { makeElement } from '../view.js'

const message_view = document.querySelector('#message-log')

const fetchLog = name =>
    fetch(`/log/${name}`)
        .then(res => res.text())
        .then(body =>
            body.split('\n')
                .filter(line => line.length > 0)
                .map(JSON.parse)
        )

const MessageType = {
    incoming: {
        title: 'Received Message',
        type: 'type-received',
    },
    outgoing: {
        title: 'Sent message',
        type: 'type-sent',
    },
}

const renderLog = ({ type, routingKey, body, timestamp }) => {
    const bodyPretty = JSON.stringify(JSON.parse(body), null, 2)
    const { title, type: msg_type } = MessageType[type]

    return makeElement('div', [ 'message', msg_type ], [
        makeElement('h4', [ 'spread' ], [
            makeElement('span', [], `${title} [${routingKey}]`),
            makeElement('span', [], new Date(timestamp).toLocaleString()),
        ]),
        makeElement('pre', [], bodyPretty),
    ])
}

fetchLog("rabbitmq.log")
    .then(logs => {
        logs.map(renderLog)
            .forEach(log => {
                message_view.appendChild(log)
            })
    })
