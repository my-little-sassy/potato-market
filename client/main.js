import {
    Alerts,
    TradeRequests,
} from './view.js'

const ws = (() => {
    const host = window.location.host
    const protocol = window.location.protocol.startsWith('https')
        ? 'wss'
        : 'ws'
    return new WebSocket(`${protocol}://${host}/ws/potato`)
})()

const client_id_view = document.querySelector('#client-id')
const post_request_form = document.querySelector('#post-request')

let posted_requests = []

const renderTradeRequests = TradeRequests(request_id => {
    posted_requests = posted_requests.filter(req => req.id != request_id)
    renderTradeRequests(posted_requests)
})


const findRequest = id =>
    Promise.resolve(posted_requests.find(request => request.id == id))
        .then(request => request != null
            ? Promise.resolve(request)
            : Promise.reject(`Could not find request ${id}`)
        )

const MessageHandler = {
    LoginResponse: ({ userId }) => {
        client_id_view.textContent = `Client id: ${userId}`
        client_id_view.classList.remove('hidden')
    },
    TradeRequestCreated: ({ request }) => {
        posted_requests = posted_requests
            .filter(({ id }) => id != request.id)
            .concat(request)
        renderTradeRequests(posted_requests)
    },
    TradeRequestOffered: ({ offerId, to }) => {
        findRequest(offerId)
            .then(request => {
                request.status = 'offered'
                request.offered = to
                return posted_requests
            })
            .then(renderTradeRequests)
    },
    TradeRequestAccepted: ({ id, by }) => {
        findRequest(id)
            .then(request => {
                request.status = 'accepted'
                request.accepted = by
                return posted_requests
            })
            .then(renderTradeRequests)
    },
    TradeOfferExpired: ({ id }) => {
        findRequest(id)
            .then(request => {
                request.status = 'open'
                request.offered = null
                return posted_requests
            })
            .then(renderTradeRequests)
    },
    _: key => message => {
        const error = `Cannot handle message of type '${key}' with content '${JSON.stringify(message, null, 2)}'`
        console.error(error)
        Alerts.addMessage('error', error)
    },
}
const handleMessage = raw => {
    const data = JSON.parse(raw)
    const [[ key, message ]] = Object.entries(data)
    return (MessageHandler[key] || MessageHandler._(key))(message)
}


ws.addEventListener('open', () => {
    console.log('WebSocket connection opened.')
    post_request_form.classList.remove('hidden')
})

ws.addEventListener('message', ev => {
    console.log(`Received message '${ev.data}'`)
    handleMessage(ev.data)
})

post_request_form.onsubmit = ev => {
    ev.preventDefault()
    const message = JSON.stringify({
        PostTradeRequest: {
            offer: post_request_form.offer.value,
            want: post_request_form.want.value,
        }
    })
    console.log(`Posting offer '${message}'`)
    ws.send(message)
}
