export const makeElement = (type, class_list = [], children = []) => {
    const element = document.createElement(type)
    element.classList.add(...class_list)
    Array.isArray(children)
        ? children.forEach(c => element.appendChild(c))
        : typeof children == 'string'
        ? element.textContent = children
        : null
    return element
}

export const span = (content, class_list = []) => makeElement('span', class_list, content)

const removeChildren = node => {
    while (node.firstChild) {
        node.removeChild(node.firstChild)
    }
}

export const Alerts = (() => {
    const alert_box = document.querySelector('#messages')

    const addMessage = (type, text) => {
        const alert = makeElement('div', [ 'alert', type ], [
            makeElement('div', [], text),
        ])
    
        const close_button = makeElement('i', [ 'close-btn' ])
        alert.appendChild(close_button)
        close_button.onclick = () => {
            alert_box.removeChild(alert)
        }
    
        alert_box.appendChild(alert)
    }
    
    const clearMessages = () => {
        removeChildren(alert_box)
    }

    return {
        addMessage,
        clearMessages,
    }
})()

export const TradeRequests = (() => {
    const trade_requests = document.querySelector('#trade-requests')

    const makeCloseButton = (id, removeRequest) => {
        const button = makeElement('i', [ 'close-btn' ])
        button.onclick = () => removeRequest(id)
        button.setAttribute('title', 'Remove from list')
        return button
    }

    const makeIcon = () => {
        const icon = makeElement('img')
        icon.setAttribute('src', 'potato.png')
        icon.setAttribute('height', '20px')
        icon.setAttribute('width', '20px')
        return icon
    }

    const makeHeader = posted_requests =>
        posted_requests.length > 0
            ? [ makeElement('div', [ 'trade-offer', 'header' ], [
                  makeElement('span'),
                  span('Id'),
                  span('Offer'),
                  span('Want'),
                  span('Offered to'),
                  span('Accepted by'),
                  makeElement('span'),
              ])]
            : []
    
    return removeRequest => posted_requests => {
        removeChildren(trade_requests)

        const header = makeHeader(posted_requests)
        const rows = posted_requests
            .map(({ id, offer, want, status = 'open', offered, accepted }) => {
                const class_list = [ 'trade-offer', status ]
                return makeElement('div', class_list, [
                    makeIcon(),
                    span(id),
                    span(offer),
                    span(want),
                    span(offered || ''),
                    span(accepted || ''),
                    accepted != null
                        ? makeCloseButton(id, removeRequest)
                        : span(''),
                ])
            })
        
        header.concat(rows)
            .forEach(request => {
                trade_requests.appendChild(request)
            })
    }
})()
