import sbt._

object Dependencies {
  private val TapirVersion = "1.0.0"
  private val CirceVersion = "0.14.1"
  private val PureconfigVersion = "0.17.1"

  lazy val cats = Seq(
    "org.typelevel" %% "cats-core"   % "2.7.0",
    "org.typelevel" %% "cats-effect" % "3.3.12",
    "co.fs2"        %% "fs2-core"    % "3.2.7"
  )

  lazy val httpServer = Seq(
    "com.softwaremill.sttp.tapir" %% "tapir-core"          % TapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-http4s-server" % TapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-json-circe"    % TapirVersion,

    "org.http4s" %% "http4s-blaze-server" % "0.23.12"
  )

  lazy val rabbitMQ = Seq(
    "com.rabbitmq" % "amqp-client" % "5.14.2"
  )

  lazy val utils = Seq(
    "com.typesafe.scala-logging" %% "scala-logging"   % "3.9.5",
    "ch.qos.logback"             %  "logback-classic" % "1.2.11",

    "com.github.pureconfig" %% "pureconfig"             % PureconfigVersion,
    "com.github.pureconfig" %% "pureconfig-cats-effect" % PureconfigVersion
  )

  lazy val json = Seq(
    "io.circe" %% "circe-core"    % CirceVersion,
    "io.circe" %% "circe-generic" % CirceVersion,
    "io.circe" %% "circe-parser"  % CirceVersion
  )
}
