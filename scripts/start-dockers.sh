#!/bin/bash

docker run --rm --name potato-rabbit \
	--env RABBITMQ_DEFAULT_USER="potato" \
	--env RABBITMQ_DEFAULT_PASS="potato" \
	-it -d --hostname potato -p 15672:15672 -p 5672:5672 \
	--network potato-market \
	rabbitmq:3-management
until $(docker exec potato-rabbit rabbitmq-diagnostics -q ping)
do
	sleep 2
done

docker run --rm -it -p 8080:8080 -d --name potato-market-1 --network potato-market potato-market:latest
docker run --rm -it -p 8081:8080 -d --name potato-market-2 --network potato-market potato-market:latest
docker run --rm -it -p 8082:8080 -d --name potato-market-3 --network potato-market potato-market:latest
