package potato

import cats.effect.kernel.Resource
import cats.effect.{ExitCode, IO, IOApp}
import pureconfig.ConfigSource
import pureconfig.generic.auto._
import pureconfig.module.catseffect.syntax._


object Main extends IOApp {

  def configResource: Resource[IO, ServerConfig] = Resource.eval {
    ConfigSource.default.loadF[IO, ServerConfig]()
  }

  override def run(args: List[String]): IO[ExitCode] = {
    val serverId = ServerId()

    val resources = for {
      config          <- configResource
      rabbitMqClient  <- RabbitMQClient.resource(serverId, config.rabbitMqUri)
      sms             = new ServerMessageSender(rabbitMqClient)
      potatoWsService <- PotatoWsService(serverId, config, sms)
      potatoApi = PotatoWsApi(potatoWsService)
      clientApi = PotatoAppApi()
      server <- ServerInterpreter(config, clientApi, potatoApi)
      _      <- potatoWsService.marketState.compile.drain.background
      _      <- potatoWsService.outgoingServerMessages.compile.drain.background
    } yield server

    resources
      .useForever
      .as(ExitCode.Success)
  }

}

case class ServerConfig(
  host: String,
  port: Int,
  rabbitMqUri: String,
  offerTtl: Int,
  offerCleanupInterval: Int
)
