package potato

import cats.effect.IO
import sttp.tapir.filesGetServerEndpoint

object PotatoAppApi {
  def apply(): List[HttpEndpoint] = {
    List(
      filesGetServerEndpoint[IO]("app")("client"),
      filesGetServerEndpoint[IO]("log")("log"),
    )
  }
}
