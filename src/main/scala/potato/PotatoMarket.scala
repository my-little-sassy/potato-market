package potato

import com.typesafe.scalalogging.LazyLogging

object PotatoMarket extends LazyLogging {
  def apply(serverId: ServerId, offerTtl: Long): (MarketStep, MarketAction) => MarketStep = {

    // Client actions
    case ((state, _), MarketAction.PostTradeRequest(id, userId, offer, want)) =>
      val request = PotatoRequest(id, userId, offer, want, Open)

      val actions = Seq(
        MarketAction.BroadcastServerMessage(BroadcastTradeRequest(id, userId, offer, want, serverId)),
        MarketAction.SendClientMessage(TradeRequestCreated(request), userId),
      )

      MarketStep(state + (id -> PotatoRequest(id, userId, offer, want, Open)), actions)

    case ((state, _), MarketAction.ClearRequests(userId)) =>
      MarketStep(state.filterNot { case (_, request) => request.userId == userId })


    // Server actions
    case ((state, _), MarketAction.OnRequestBroadcast(requestId, userId, offer, want, origin)) =>
      state
        .find { case (_, request) =>
          request.offer == want && request.want == offer && request.state == Open
        }
        .map { case (_, request) => request }
        .map { request =>
          val updatedState = state + (request.id -> request.copy(state = Offered(requestId, userId, currentTimestamp)))
          val actions = Seq(
            MarketAction.SendServerMessage(OfferTradeRequest(requestId, request.id, request.userId, origin, serverId), origin),
            MarketAction.SendClientMessage(TradeRequestOffered(requestId, request.id, userId), request.userId),
          )
          MarketStep(updatedState, actions)
        }
        .getOrElse(MarketStep(state))

    case ((state, _), MarketAction.OnOfferTrade(requestId, offerId, fromUser, fromServer)) =>
      state.get(requestId)
        .filter(_.state == Open)
        .map { request =>
          val updatedState = state - request.id
          val actions = Seq(
            MarketAction.SendServerMessage(AckTradeRequest(requestId, offerId, fromServer, serverId), fromServer),
            MarketAction.SendClientMessage(TradeRequestAccepted(requestId, fromUser), request.userId),
          )

          MarketStep(updatedState, actions)
        }
        .getOrElse(MarketStep(state))

    case ((state, _), MarketAction.OnAckTrade(requestId, offerId, fromServer)) =>
      state.get(offerId)
        .filter(_.state match {
          case _: Offered => true
          case _          => false
        })
        .map { request =>
          val updatedState = state - request.id
          val acceptedBy = request.state match {
            case Offered(_, userId, _) => userId
            case _                     => UserId.empty
          }
          val actions = Seq(
            MarketAction.SendClientMessage(TradeRequestAccepted(request.id, acceptedBy), request.userId),
          )

          MarketStep(updatedState, actions)
        }
        .getOrElse(MarketStep(state))


    // Internal actions
    case ((state, _), MarketAction.ClearExpiredOffers) =>
      state.foldLeft(MarketStep()) { case ((state, actions), (requestId, request)) =>
        request.state match {
          case Offered(_, _, timestamp) if timestamp + offerTtl < currentTimestamp =>
            val updatedState = state + (requestId -> request.copy(state = Open))
            val newActions = Seq(
              MarketAction.SendClientMessage(TradeOfferExpired(requestId), request.userId)
            )
            MarketStep(updatedState, actions ++ newActions)

          case _ =>
            MarketStep(state + (requestId -> request), actions)
        }
      }


    // Ignored actions
    case ((state, _), MarketAction.SendClientMessage(_, _)) => MarketStep(state)
    case ((state, _), MarketAction.SendServerMessage(_, _)) => MarketStep(state)
  }
}
