package potato

import cats.effect.IO
import fs2.Pipe
import io.circe.generic.auto._
import sttp.capabilities.WebSockets
import sttp.capabilities.fs2.Fs2Streams
import sttp.tapir._
import sttp.tapir.generic.auto._
import sttp.tapir.json.circe.TapirJsonCirce
import sttp.tapir.server.ServerEndpoint

object PotatoWsApi extends Tapir with TapirJsonCirce {

  val potato: PublicEndpoint[Unit, Unit, Pipe[IO, WsIncomingMessage, WsOutgoingMessage], Fs2Streams[IO] with WebSockets] =
    endpoint.get
      .in("potato")
      .out(webSocketBody[WsIncomingMessage, CodecFormat.Json, WsOutgoingMessage, CodecFormat.Json](Fs2Streams[IO]))

  def apply(service: PotatoWsService): List[ServerEndpoint[Fs2Streams[IO] with WebSockets, IO]] = {
    List(
      potato.serverLogicSuccess(_ => IO(service.potatoLogic))
    )
  }
}
