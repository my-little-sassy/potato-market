package potato

import cats.effect.{IO, Temporal}
import cats.effect.kernel.Resource
import cats.effect.std.Queue
import com.typesafe.scalalogging.LazyLogging
import fs2.{Pipe, Stream}
import fs2.concurrent.Topic

import scala.concurrent.duration._

class PotatoWsService(
    serverId: ServerId,
    config: ServerConfig,
    inbound: Queue[IO, MarketAction],
    topic: Topic[IO, MarketStep],
    serverMessageSender: ServerMessageSender
  )(implicit F: Temporal[IO]) extends LazyLogging {

  private val incomingServerMessages: Stream[IO, MarketAction] = serverMessageSender.incoming
    .collect {
      case BroadcastTradeRequest(requestId, userId, offer, want, origin) =>
        MarketAction.OnRequestBroadcast(requestId, userId, offer, want, origin)
      case OfferTradeRequest(requestId, offerId, offeredBy, to, origin) =>
        MarketAction.OnOfferTrade(requestId, offerId, offeredBy, origin)
      case AckTradeRequest(requestId, offerId, to, origin) =>
        MarketAction.OnAckTrade(requestId, offerId, origin)
    }

  private val cleanUpTask: Stream[IO, MarketAction] = Stream.awakeEvery(config.offerCleanupInterval.seconds)
    .as(MarketAction.ClearExpiredOffers)

  val marketState: Stream[IO, Nothing] = Stream.fromQueueUnterminated(inbound)
    .merge(incomingServerMessages)
    .merge(cleanUpTask)
    .evalTap { action => IO {
      logger.debug(s"Action received: $action")
    }}
    .scan(MarketStep()) (PotatoMarket(serverId, config.offerTtl.seconds.toMillis))
    .evalTap { case (state, actions) => IO {
      logger.debug(
        s"""Current state:
           |${state.mkString("\n")}
           |Actions:
           |${actions.mkString("\n")}
           |""".stripMargin)
    }}
    .through(topic.publish)

  val outgoingServerMessages: Stream[IO, Unit] = serverMessageSender.outgoing {
    topic.subscribe(100)
      .flatMap {
        case (_, actions) => Stream.emits(actions)
      }
  }

  val potatoLogic: Pipe[IO, WsIncomingMessage, WsOutgoingMessage] = { in =>
    val clientId = UserId()
    logger.info(s"[$clientId] Opened new connection for client")

    val incoming = in
      .evalTap { msg => IO {
        logger.debug(s"[$clientId] Received message $msg")
      }}
      .collect {
        case PostTradeRequest(offer, want) =>
          MarketAction.PostTradeRequest(RequestId(), clientId, offer, want)
      }
      .evalTap { msg =>
        inbound.offer(msg)
      }
      .filter(_ => false)
      .as(LoginResponse(clientId))

    val outgoing: Stream[IO, WsOutgoingMessage] = topic.subscribe(100)
      .flatMap {
        case (_, actions) => Stream.emits(actions)
      }
      .collect {
        case MarketAction.SendClientMessage(message, userId) if userId == clientId =>
          message
      }

    Stream.emit(LoginResponse(clientId) : WsOutgoingMessage)
      .merge(outgoing)
      .mergeHaltR(incoming)
      .evalTap { message => IO {
        logger.debug(s"[$clientId] Sending message $message")
      }}
      .onFinalize(for {
        _ <- IO(logger.info(s"[$clientId] Closed connection for client '$clientId'"))
        _ <- inbound.offer(MarketAction.ClearRequests(clientId))
      } yield {})
  }
}

object PotatoWsService {
  def apply(serverId: ServerId, config: ServerConfig, sms: ServerMessageSender): Resource[IO, PotatoWsService] = {
    val acquire: IO[PotatoWsService] = for {
      q <- Queue.unbounded[IO, MarketAction]
      t <- Topic[IO, MarketStep]
    } yield new PotatoWsService(serverId, config, q, t, sms)
    Resource.make(acquire)(_ => IO.unit)
  }
}
