package potato

import java.io.FileWriter

import cats.effect.IO
import cats.effect.kernel.Resource
import cats.effect.std.Queue
import cats.effect.unsafe.implicits.global
import com.rabbitmq.client._
import com.typesafe.scalalogging.LazyLogging
import fs2.Stream
import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._

import scala.util.Random

class ServerMessageSender(client: RabbitMQClient) {

  def outgoing(messages: Stream[IO, MarketAction]): Stream[IO, Unit] = {
    messages.collect {
      case MarketAction.SendServerMessage(message, routingKey) =>
        client.sendMessage(message, routingKey)
    }
  }

  val incoming: Stream[IO, ServerMessage] = client.subscribe

}

object RabbitMQConfig {
  val ExchangeName: String = "potato-exchange"
  val QueueName: String = s"potato-queue-${Random.alphanumeric.take(16).mkString}"
  val BroadcastKey: String = "potato-broadcast"
}

class RabbitMQClient(serverId: ServerId, rabbitMqUri: String) extends LazyLogging with AutoCloseable {
  import RabbitMQConfig._
  private val RoutingKey = serverId

  private val MessageProperties = new AMQP.BasicProperties.Builder()
    .contentType("application/json")
    .build()

  private val connection = {
    val connFactory = new ConnectionFactory
    connFactory.setUri(rabbitMqUri)
    connFactory.newConnection()
  }

  private val channel = connection.createChannel()

  channel.exchangeDeclare(ExchangeName, "direct", true)
  channel.queueDeclare(QueueName, true, false, false, null)
  channel.queueBind(QueueName, ExchangeName, BroadcastKey)
  channel.queueBind(QueueName, ExchangeName, RoutingKey)

  def sendMessage(message: ServerMessage, routingKey: String): Unit = {
    channel.basicPublish(ExchangeName, routingKey, MessageProperties, message.asJson.noSpaces.getBytes)
    FileLogger.outgoing(routingKey, message.asJson.noSpaces)
  }

  def subscribe: Stream[IO, ServerMessage] = {
    for {
      q <- Stream.eval(Queue.unbounded[IO, ServerMessage])
      _ <- Stream.eval(IO {
        def enqueue(m: ServerMessage): Unit = q.offer(m).unsafeRunAndForget()

        channel.basicConsume(QueueName, true, serverId, new DefaultConsumer(channel) {
          override def handleDelivery(consumerTag: ServerId, envelope: Envelope, properties: AMQP.BasicProperties, body: Array[Byte]): Unit = {
            val routingKey = envelope.getRoutingKey
            val deliveryTag = envelope.getDeliveryTag
            val bodyStr = new String(body)

            logger.info(
              s"""Received message
                 |Routing key:  $routingKey
                 |Delivery tag: $deliveryTag
                 |Message raw:  $bodyStr
                 |""".stripMargin)
            FileLogger.incoming(routingKey, bodyStr)

            decode[ServerMessage](bodyStr) match {
              case Right(message) => enqueue(message)
              case Left(error) =>
                logger.error(s"Error while decoding message $bodyStr", error)
            }
          }
        })
      })
      data <- Stream.fromQueueUnterminated(q)
    } yield data
  }

  private object FileLogger {
    private val FileName = "log/rabbitmq.log"

    case class LogLine(`type`: String, routingKey: String, body: String, timestamp: Long)

    private def log(`type`: String, routingKey: String, body: String): Unit = {
      val file = new FileWriter(FileName, true)
      file.write(LogLine(`type`, routingKey, body, currentTimestamp).asJson.noSpaces + "\n")
      file.close()
    }

    val incoming: (String, String) => Unit = log("incoming", _, _)
    val outgoing: (String, String) => Unit = log("outgoing", _, _)
  }

  override def close(): Unit = {
    channel.close()
    connection.close()
  }
}

object RabbitMQClient {
  def resource(serverId: ServerId, rabbitMqUri: String): Resource[IO, RabbitMQClient] = {
    Resource.fromAutoCloseable(IO(new RabbitMQClient(serverId, rabbitMqUri)))
  }
}
