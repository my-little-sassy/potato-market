package potato

import cats.effect.IO
import cats.effect.kernel.Resource
import org.http4s.HttpApp
import org.http4s.blaze.server.BlazeServerBuilder
import org.http4s.server.websocket.WebSocketBuilder2
import org.http4s.server.{Router, Server}
import sttp.tapir.server.http4s.Http4sServerInterpreter

object ServerInterpreter {
  private def bindWebSocketApp(httpEndpoints: List[HttpEndpoint], wsEndpoints: List[WsEndpoint])(wsb: WebSocketBuilder2[IO]): HttpApp[IO] = {
    val http4sServerInterpreter = Http4sServerInterpreter[IO]()
    val httpRoutes = http4sServerInterpreter.toRoutes(httpEndpoints)
    val wsRoutes = http4sServerInterpreter.toWebSocketRoutes(wsEndpoints)(wsb)

    Router(
      "/"   -> httpRoutes,
      "/ws" -> wsRoutes
    ).orNotFound
  }

  def apply(config: ServerConfig, httpEndpoints: List[HttpEndpoint], wsEndpoints: List[WsEndpoint]): Resource[IO, Server] = {
    BlazeServerBuilder[IO]
      .bindHttp(config.port, config.host)
      .withHttpWebSocketApp(bindWebSocketApp(
        httpEndpoints,
        wsEndpoints
      ))
      .resource
  }
}
