import cats.effect.IO
import sttp.capabilities.WebSockets
import sttp.capabilities.fs2.Fs2Streams
import sttp.tapir.json.circe.TapirJsonCirce
import sttp.tapir.server.ServerEndpoint

import scala.util.Random

package object potato extends TapirJsonCirce {

  type HttpEndpoint = ServerEndpoint[Any, IO]
  type WsEndpoint = ServerEndpoint[Fs2Streams[IO] with WebSockets, IO]

  type UserId = String
  object UserId {
    def apply(): UserId = Random.alphanumeric.take(8).mkString

    def empty: UserId = ""
  }

  type RequestId = String
  object RequestId {
    def apply(): RequestId = Random.alphanumeric.take(16).mkString
  }

  type PotatoKind = String

  type ServerId = String
  object ServerId {
    def apply(): ServerId = Random.alphanumeric.take(8).mkString
  }


  // WebSocket messages

  sealed trait WsIncomingMessage
  case class PostTradeRequest(offer: PotatoKind, want: PotatoKind) extends WsIncomingMessage

  sealed trait WsOutgoingMessage
  case class LoginResponse(userId: UserId) extends WsOutgoingMessage
  case class TradeRequestCreated(request: PotatoRequest) extends WsOutgoingMessage
  case class TradeRequestOffered(id: RequestId, offerId: RequestId, to: UserId) extends WsOutgoingMessage
  case class TradeRequestAccepted(id: RequestId, by: UserId) extends WsOutgoingMessage
  case class TradeOfferExpired(id: RequestId) extends WsOutgoingMessage


  // RabbitMQ messages

  sealed trait ServerMessage
  case class BroadcastTradeRequest(requestId: RequestId, userId: UserId, offer: PotatoKind, want: PotatoKind, origin: ServerId) extends ServerMessage
  case class OfferTradeRequest(requestId: RequestId, offerId: RequestId, offeredBy: UserId, to: ServerId, origin: ServerId) extends ServerMessage
  case class AckTradeRequest(requestId: RequestId, offerId: RequestId, to: ServerId, origin: ServerId) extends ServerMessage


  // Market actions

  sealed trait MarketAction
  object MarketAction {
    case class ClearRequests(userId: UserId) extends MarketAction
    case class PostTradeRequest(id: RequestId, userId: UserId, offer: PotatoKind, want: PotatoKind) extends MarketAction

    case class OnRequestBroadcast(id: RequestId, userId: UserId, offer: PotatoKind, want: PotatoKind, origin: ServerId) extends MarketAction
    case class OnOfferTrade(requestId: RequestId, offerId: RequestId, fromUser: UserId, fromServer: ServerId) extends MarketAction
    case class OnAckTrade(requestId: RequestId, offerId: RequestId, fromUser: UserId) extends MarketAction

    case class SendServerMessage(message: ServerMessage, routingKey: String) extends MarketAction
    def BroadcastServerMessage(message: ServerMessage): MarketAction = SendServerMessage(message, RabbitMQConfig.BroadcastKey)
    case class SendClientMessage(message: WsOutgoingMessage, userId: UserId) extends MarketAction

    case object ClearExpiredOffers extends MarketAction
  }


  // Market state

  sealed trait PotatoRequestState
  case object Open extends PotatoRequestState
  case class Offered(requestId: RequestId, userId: UserId, timestamp: Long) extends PotatoRequestState

  case class PotatoRequest(id: RequestId, userId: UserId, offer: PotatoKind, want: PotatoKind, state: PotatoRequestState)
  type MarketState = Map[RequestId, PotatoRequest]
  object MarketState {
    def apply(): MarketState = Map[RequestId, PotatoRequest]()
  }

  type MarketStep = (MarketState, Seq[MarketAction])
  object MarketStep {
    def apply(): MarketStep = (MarketState(), Seq.empty)
    def apply(state: MarketState): MarketStep = (state, Seq.empty)
    def apply(state: MarketState, actions: Seq[MarketAction]): MarketStep = (state, actions)
  }

  def currentTimestamp: Long = System.currentTimeMillis()
}
